﻿using System;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Crear un programa que muestre las letras de la Z (mayúscula) a la A (mayúscula, descendiendo).\n");
            Char ab = 'Z';
            do{
                Console.WriteLine("Letra: "+ab);
                ab--;    
            }while(ab >= 'A'); 
            }
        }
    }

