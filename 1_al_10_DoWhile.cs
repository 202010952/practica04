﻿using System;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Crea un programa que escriba en pantalla los números del 1 al 10, usando do while\n");
            int num=1;
            do{
               Console.WriteLine(num); 
               num++;
            }while(num <=10);
        }
    }
}
