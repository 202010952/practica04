using System;

namespace Practica_3_C_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Crear un programa que pida números positivos al usuario, y vaya calculando la suma de todos ellos(terminará cuando se teclea un número negativo o cero).\n");

            int num, suma=0;
            do{
                    Console.Write("Favor introducir un numero posito (Programa dejara de funcionar si ingresa un numero negativo o cero)\n");
                    num = int.Parse(Console.ReadLine());
                    suma = suma+num;
                    if(num < 0){
                        Console.Write("El numero ingresado: "+num+" es negativo\n");
                        break;
                    }
                    else{
                         Console.WriteLine(" La Suma de valor ingresado es igual a: " + suma + " \n");
                    }
                    
            }while (num >=1);
               
        }
    }
}
